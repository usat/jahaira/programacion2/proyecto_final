<?php

require_once '../datos/Conexion.clase.php';

class Compra extends Conexion
{

    private $numeroCompra;
    private $codigoTipoComprobante;
    private $rucProveedor;
    private $numeroSerie;
    private $numero_documento;
    private $fechaCompra;
    private $porcentajeIGV;
    private $subTotal;
    private $igv;
    private $total;
    private $codigoUsuario;
    private $estado;
    private $compraDetalle;
    function getNumeroCompra()
    {
        return $this->numeroCompra;
    }

    function getCodigoTipoComprobante()
    {
        return $this->codigoTipoComprobante;
    }

    function getRucProveedor()
    {
        return $this->rucProveedor;
    }

    function getNumeroSerie()
    {
        return $this->numeroSerie;
    }

    function getNumero_documento()
    {
        return $this->numero_documento;
    }

    function getFechaCompra()
    {
        return $this->fechaCompra;
    }

    function getPorcentajeIGV()
    {
        return $this->porcentajeIGV;
    }

    function getSubTotal()
    {
        return $this->subTotal;
    }

    function getIgv()
    {
        return $this->igv;
    }

    function getTotal()
    {
        return $this->total;
    }

    function getCodigoUsuario()
    {
        return $this->codigoUsuario;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function getCompraDetalle()
    {
        return $this->compraDetalle;
    }

    function setNumeroCompra($numeroCompra)
    {
        $this->numeroCompra = $numeroCompra;
    }

    function setCodigoTipoComprobante($codigoTipoComprobante)
    {
        $this->codigoTipoComprobante = $codigoTipoComprobante;
    }

    function setRucProveedor($rucProveedor)
    {
        $this->rucProveedor = $rucProveedor;
    }

    function setNumeroSerie($numeroSerie)
    {
        $this->numeroSerie = $numeroSerie;
    }

    function setNumero_documento($numero_documento)
    {
        $this->numero_documento = $numero_documento;
    }

    function setFechaCompra($fechaCompra)
    {
        $this->fechaCompra = $fechaCompra;
    }

    function setPorcentajeIGV($porcentajeIGV)
    {
        $this->porcentajeIGV = $porcentajeIGV;
    }

    function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    function setIgv($igv)
    {
        $this->igv = $igv;
    }

    function setTotal($total)
    {
        $this->total = $total;
    }

    function setCodigoUsuario($codigoUsuario)
    {
        $this->codigoUsuario = $codigoUsuario;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
    }

    function setCompraDetalle($compraDetalle)
    {
        $this->compraDetalle = $compraDetalle;
    }



    public function agregar()
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "select 
                        c.numero+1  as nc
                    from 
                        correlativo c 
                    where 
                        c.tabla = 'compra'";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetch();

            if ($sentencia->rowCount()) {
                $nuevoNumeroCompra = $resultado["nc"];
                $this->setNumeroCompra($nuevoNumeroCompra);


                $sql = "
                        INSERT INTO compra(
            numero_compra, codigo_tipo_comprobante, ruc_proveedor, numero_serie, 
            numero_documento, fecha_compra, porcentaje_igv, sub_total, igv, 
            total, codigo_usuario)
    VALUES (
 :p_numero_compra, :p_codigo_tipo_comprobante, :p_ruc_proveedor, :p_numero_serie, 
            :p_numero_documento, :p_fecha_compra, :p_porcentaje_igv, :p_sub_total, :p_igv, 
            :p_total, :p_codigo_usuario
    );

                    ";

                //Preparar la sentencia
                $sentencia = $this->dblink->prepare($sql);

                //Asignar un valor a cada parametro
                $sentencia->bindValue(":p_numero_compra", $this->getNumeroCompra());
                $sentencia->bindValue(":p_codigo_tipo_comprobante", $this->getCodigoTipoComprobante());
                $sentencia->bindValue(":p_ruc_proveedor", $this->getRucProveedor());
                $sentencia->bindValue(":p_numero_serie", $this->getNumeroSerie());
                $sentencia->bindValue(":p_numero_documento", $this->getNumero_documento());
                $sentencia->bindValue(":p_fecha_compra", $this->getFechaCompra());
                $sentencia->bindValue(":p_porcentaje_igv", $this->getPorcentajeIgv());
                $sentencia->bindValue(":p_sub_total", $this->getSubTotal());
                $sentencia->bindValue(":p_igv", $this->getIgv());
                $sentencia->bindValue(":p_total", $this->getTotal());
                $sentencia->bindValue(":p_codigo_usuario", $this->getCodigoUsuario());

                //Ejecutar la sentencia preparada
                $sentencia->execute();


                /*INSERTAR EN LA TABLA VENTA_DETALLE*/
                $detalleCompraArray = json_decode($this->getCompraDetalle()); //Convertir de formato JSON a formato array

                $item = 0;

                foreach ($detalleCompraArray as $key => $value) { //permite recorrer el array

                    //                    $sql = "select stock, nombre from articulo where codigo_articulo = :p_codigo_articulo";
                    //                    $sentencia = $this->dblink->prepare($sql);
                    //                    $sentencia->bindValue(":p_codigo_articulo", $value->codigoArticulo);
                    //		    $sentencia->execute();
                    //                    $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
                    //                    if ($resultado["stock"] < $value->cantidad){
                    //                        throw new Exception("No hay stock suficiente" . "\n" . "Artículo: " . $value->codigoArticulo . " - " . $resultado["nombre"] . "\n" . "Stock actual: " . $resultado["stock"] . "\n" . "Cantidad de venta: " . $value->cantidad);
                    //                    }
                    //                    

                    $sql = "
                           INSERT INTO compra_detalle(
            numero_compra, 
            codigo_articulo, 
            item,
            cantidad, 
            precio, 
            descuento)
    VALUES (:p_numero_compra, 
            :p_codigo_articulo, 
            :p_item,
            :p_cantidad, 
            :p_precio, 
            :p_descuento);

                        ";


                    //Preparar la sentencia
                    $sentencia = $this->dblink->prepare($sql);

                    $item++;

                    //Asignar un valor a cada parametro
                    $sentencia->bindValue(":p_numero_compra", $this->getNumeroCompra());

                    $sentencia->bindValue(":p_codigo_articulo", $value->codigoArticulo);
                    $sentencia->bindValue(":p_item", $item);
                    $sentencia->bindValue(":p_cantidad", $value->cantidad);
                    $sentencia->bindValue(":p_precio", $value->precio);
                    $descuento = 0;
                    $sentencia->bindValue(":p_descuento", $descuento);






                    //Ejecutar la sentencia preparada
                    $sentencia->execute();


                    /*ACTUALIZAR EL STOCK DE CADA ARTICULO VENDIDO*/
                    $sql = "update articulo 
                            set stock = stock + :p_cantidad 
                            where codigo_articulo = :p_codigo_articulo";

                    $sentencia = $this->dblink->prepare($sql);
                    $sentencia->bindValue(":p_codigo_articulo", $value->codigoArticulo);
                    $sentencia->bindValue(":p_cantidad", $value->cantidad);
                    $sentencia->execute();
                    /*ACTUALIZAR EL STOCK DE CADA ARTICULO VENDIDO*/
                }
                /*INSERTAR EN LA TABLA VENTA_DETALLE*/


                //Actualizar el correlativo en +1
                $sql = "update correlativo set numero = numero + 1 where tabla = 'compra'";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->execute();
                // actualizar ek correlativo segun el tipo de documento y la serie
                $sql = "update serie_comprobante set numero_documento = numero_documento + 1 
                        where codigo_tipo_comprobante = :p_codigo_tipo_comprobante and numero_serie = :p_numero_serie";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindValue(":p_codigo_tipo_comprobante", $this->getCodigoTipoComprobante());
                $sentencia->bindValue(":p_numero_serie", $this->getNumeroSerie());
                $sentencia->execute();
                //Terminar la transacción
                $this->dblink->commit();


                return true;
            }
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }


    public function listar($fecha1, $fecha2, $tipo)
    {
        try {
            $sql = "SELECT 
			  compra.numero_compra as nro_compra, 
			  tipo_comprobante.descripcion as tipo_doc, 
			  compra.numero_serie as serie, 
			  compra.numero_documento as documento, 
			  compra.ruc_proveedor as ruc, 
			  proveedor.razon_social as razon_social, 
			  compra.fecha_compra as fecha, 
			  compra.sub_total as sub_total, 
			  compra.igv, 
			  compra.total, 
			  compra.porcentaje_igv as impuesto, 
			  usuario.dni_usuario as dni_usuario, 
			  compra.fecha_registro, 
			  compra.hora_registro, 
			  (case when compra.estado = 'E' then 'Emitido' else 'Anulado' end) as estado
			FROM 
			  compra, 
			  proveedor, 
			  usuario, 
			  tipo_comprobante
			WHERE 
			  proveedor.ruc_proveedor = compra.ruc_proveedor 
			  and usuario.codigo_usuario = compra.codigo_usuario 
			  and tipo_comprobante.codigo_tipo_comprobante = compra.codigo_tipo_comprobante

			  and 
			  (
				case :p_tipo
					when 1 then compra.fecha_compra = current_date
					when 2 then compra.fecha_compra between :p_fecha1 and :p_fecha2
					else 
						1=1
				end
			  )  
			ORDER BY
			  compra.numero_compra ASC;";
            // $sql = "select * from f_listado_compra(:p_fecha1, :p_fecha2, :p_tipo)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_fecha1", $fecha1);
            $sentencia->bindValue(":p_fecha2", $fecha2);
            $sentencia->bindValue(":p_tipo", $tipo);
            $sentencia->execute();

            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function anular($numeroCompra)
    {
        $this->dblink->beginTransaction();
        try {
            $sql = "update compra set estado = 'A' where numero_compra = :p_numero_compra";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_numero_compra", $numeroCompra);
            $sentencia->execute();

            $sql = "select codigo_articulo, cantidad from compra_detalle where numero_compra = :p_numero_compra";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_numero_compra", $numeroCompra);
            $sentencia->execute();

            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            for ($i = 0; $i < count($resultado); $i++) {
                $sql = "update articulo set stock = stock - :p_cantidad where codigo_articulo = :p_codigo_articulo";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindValue(":p_cantidad", $resultado[$i]["cantidad"]);
                $sentencia->bindValue(":p_codigo_articulo", $resultado[$i]["codigo_articulo"]);
                $sentencia->execute();
            }

            //Terminar la transacción
            $this->dblink->commit();

            return true;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    //     public function compraReporte($fecha1,$fecha2,$tipo,$p_rucProveedor,$p_codigoTipoComprobante){

    //           try{
    //             $sql = "SELECT c.numero_compra, tc.descripcion, c.numero_serie, c.ruc_proveedor, p.razon_social as proveedor, c.fecha_compra, c.sub_total, c.igv, c.total, c.codigo_usuario, c.estado
    // from compra c inner join proveedor p on (c.ruc_proveedor = p.ruc_proveedor)
    // inner join tipo_comprobante tc on (c.codigo_tipo_comprobante = tc.codigo_tipo_comprobante)
    // 		where		
    // 		(
    // 			case :p_tipo
    // 				when 1 then c.fecha_compra = current_date --solo hoy
    // 				when 2 then c.fecha_compra >= :p_fecha1 and c.fecha_compra <= :p_fecha2 
    // 				else
    // 					true
    // 		end
    // 		)
    // 		and 
    // 		(tc.codigo_tipo_comprobante = :p_codigo_tipo_comprobante)
    // 		and
    // 		(c.ruc_proveedor= :p_ruc_proveedor)


    // 		order by
    // 			c.numero_compra;";
    //             // $sql = "select * from f_listar_compra(:p_fecha1, :p_fecha2, :p_tipo ,:p_ruc_proveedor, :p_codigo_tipo_comprobante)";

    //             $sentencia = $this->dblink->prepare($sql);
    //             $sentencia->bindValue(":p_fecha1", $fecha1);
    //             $sentencia->bindValue(":p_fecha2", $fecha2);
    //             $sentencia->bindValue(":p_tipo", $tipo);
    //             $sentencia->bindValue(":p_ruc_proveedor", $p_rucProveedor);
    //             $sentencia->bindValue(":p_codigo_tipo_comprobante", $p_codigoTipoComprobante);
    //             $sentencia->execute();

    //             $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
    //             return $resultado;

    //        } catch (Exception $exc) {
    //            throw $exc;
    //        }
    //             }

    //     public function graficoReporteCompra(){
    //         try {
    //             $sql =" 
    //   select sum(total),
    // 	razon_social

    // 	from compra c inner join proveedor p on c.ruc_proveedor = p.ruc_proveedor
    // 	group by
    // 	razon_social";

    //             $sentencia = $this->dblink->prepare($sql);
    //             $sentencia->execute();
    //             $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
    //             return $resultado;

    //         } catch (Exception $ex) {

    //         }
    //     }

}
