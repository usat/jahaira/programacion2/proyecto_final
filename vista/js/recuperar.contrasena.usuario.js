$("#btncerrar").click(function() {
	document.location.href = "index.php"
})

$("#frmgrabar").submit(function(evento) {
	evento.preventDefault()
	swal(
		{
			title: "Confirme",
			text: "¿Esta seguro de grabar los datos ingresados?",
			showCancelButton: true,
			confirmButtonColor: "#3d9205",
			confirmButtonText: "Si",
			cancelButtonText: "No",
			closeOnConfirm: false,
			closeOnCancel: true,
			imageUrl: "../imagenes/pregunta.png"
		},
		function(isConfirm) {
			if (isConfirm) {
				//el usuario hizo clic en el boton SI

				//procedo a grabar

				let email = $("#txtemail").val();

				$.post("../service/cliente.comprobar.email2.php", {
					p_email: email,
					cbotipo: $("#cbotipo").val()
				})
					.done(function(resultado) {


						var datosJSON = resultado
						if (datosJSON.datos.email != null || datosJSON.datos.dni != null) {

							$.post("../service/cliente.recuperar.contrasena.php", {
								p_email: email,
								cbotipo: $("#cbotipo").val()
							}).done(function(resultado) {


								Email.send({
									SecureToken : "9c394b7f-acf6-49fd-b90e-7a7f7d487f41",
									To : email,
									From : 'jr9002459@gmail.com',
									Subject : "Recuperación de Contraseñma",
									Body : `Su nueva contraseña es ${resultado.mensaje}`
								}).then(
									message => {
										if(message == "OK"){
											swal("Enviado...", "se envio al correo su nueva contraseña", "success")
											document.location.href = "index.php"
										}
									}
								)


							})

							// swal("Enviado...", "Verificar correo", "success")
							// document.location.href = "index.php"

						} else {
							swal("No se encontraron los datos...", "", "warning")
							$("#txtemail").val("")
							$("#cbotipo").val("")
						}



					})
					.fail(function(error) {
						var datosJSON = $.parseJSON(error.responseText)
						swal("Error", datosJSON.mensaje, "error")
					})
			}
		}
	)
})
