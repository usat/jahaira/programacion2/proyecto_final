<?php

require_once '../negocio/CambioContrasena.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {
    $tipo = $_POST["cbotipo"];
    $email = $_POST["p_email"];

    $ob = new CambioContrasena();
    $nuevaContraseña = $ob->nuevaContra($email, $tipo);

    // echo '<pre>';
    // print_r($nuevaContraseña);
    // echo '</pre>';

    Funciones::imprimeJSON(200, $nuevaContraseña, "");
} catch (Exception $exc) {
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
